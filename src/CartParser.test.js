import CartParser from './CartParser';
import path from 'path';
import * as uuid from 'uuid';

jest.mock('uuid');

const pathCSV = path.join(__dirname + '/../samples/cart.csv');
let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	describe('parse', () => {
		it('should return error when validation is not successful', () => {
			parser.validate = jest.fn();
			parser.validate.mockReturnValueOnce(['not empty array', 'exist errors']);

			expect(() => parser.parse(pathCSV)).toThrowError('Validation failed!');
		});
	});

	describe('validate', () => {
		it('should return empty array when validation is successful', () => {
			const
				fileCSV = parser.readFile(pathCSV),
				resultValidate = parser.validate(fileCSV);

			expect(resultValidate).toEqual([]);
		});

		it('should return not empty array when validation is not successful', () => {
			const
				fileCSV = parser.readFile(pathCSV),
				failFileCSV = `${fileCSV}\n some not valid data`,
				resultValidate = parser.validate(failFileCSV);

			expect(resultValidate).not.toHaveLength(0);
		});
	});

	describe('parseLine', () => {
		it('should return an object with keys from column keys and values from CSV', () => {
			const
				inputLine = 'Scelerisque lacinia,18.90,1',
				outputObject = parser.parseLine(inputLine),
				expectedObject = {
					name: 'Scelerisque lacinia',
					price: 18.9,
					quantity: 1
				};

			expect(outputObject).toMatchObject(expectedObject);
		});

		it('should return cell type number or string depending at data scheme', () => {
			const
				inputLine = 'Scelerisque lacinia,18.90,1',
				outputLine = parser.parseLine(inputLine);
			delete outputLine.id;

			let checkTypeCells = true;
			Object.values(outputLine).forEach((cell, i) => {
				const type =
					parser.schema.columns[i].type === parser.ColumnType.NUMBER_POSITIVE ?
						'number' : 'string';
				if (typeof cell !== type) {
					checkTypeCells = false;
				}
			});

			expect(checkTypeCells).toBe(true);
		});
	});

	describe('calcTotal', () => {
		it('should return zero when cart is empty', () => {
			expect(parser.calcTotal([])).toBe(0);
		});
	});
});

describe('CartParser - integration test', () => {
	it('should return JSON object with cart items and total price', () => {
		const
			pathJSON = path.join(__dirname + '/../samples/cart.json'),
			outputObject = JSON.parse(parser.readFile(pathJSON));

		let idNumber = 0;

		uuid.v4.mockImplementation(() => outputObject.items[idNumber++].id);
		const expectedObject = parser.parse(pathCSV);

		/**
		 * взагалі тест має провалитись, оскільки повертає не json, а object
		 * нуу можливо так і задумувалось, оскільки структура дуже подібна,
		 * тому я порівняв їх об'єкти
		 * якщо ж зробити перевірку у форматі json - 
		 * @compare expect(JSON.stringify(expectedObject).toEqual(JSON.stringify(outputObject));
		 * також тест не пройде, оскільки поля змінені місцями,
		 * це звісно можна було легко пофіксити перенесши рядок присвоєння айді перед циклом,
		 * але це вже зміна коду...
		 * що і збило з толку, тому і тестував objects
		 */
		expect(expectedObject).toEqual(outputObject);
	});
});